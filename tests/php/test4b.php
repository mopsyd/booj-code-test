<?php
require_once __DIR__ . '/../../src/php/equilibriumIndexes.php';
require_once __DIR__ . '/../../src/php/equilibriumTest.php';

$array = [
    0 => -7, 
    1 => 1, 
    2 => new \stdClass(), 
    3 => 2, 
    4 => -4, 
    5 => 3, 
    6 => 0
];

echo '<h1>Test Case 4b</h1>';
echo '<p>Invalid Values</p>';
echo '<p>Verbose output (message returned)</p>';
echo '<p>Test array: ' . print_r($array, 1) . '<p><hr>';

var_dump(testEquilibriums($array, 1));
