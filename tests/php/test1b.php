<?php
require_once __DIR__ . '/../../src/php/equilibriumIndexes.php';
require_once __DIR__ . '/../../src/php/equilibriumTest.php';

$array = [
    1, 2, 3, 7, 9, 4, 12, 6, 3, 2, 8, 10, 12
];

echo '<h1>Test Case 1b</h1>';
echo '<p>No Equilibriums</p>';
echo '<p>Verbose output</p>';
echo '<p>Test array: ' . print_r($array, 1) . '<p><hr>';

var_dump(testEquilibriums($array, 1));