<?php
require_once __DIR__ . '/../../src/php/equilibriumIndexes.php';
require_once __DIR__ . '/../../src/php/equilibriumTest.php';

$array = [
    0 => -7, 
    1 => 1, 
    "foobar" => 5, 
    3 => 2, 
    4 => -4, 
    5 => 3, 
    6 => 0
];

echo '<h1>Test Case 3a</h1>';
echo '<p>Invalid Keys</p>';
echo '<p>Standard output (exception thrown)</p>';
echo '<p>Test array: ' . print_r($array, 1) . '<p><hr>';

try {
var_dump(testEquilibriums($array));
} catch (\Exception $e) {
    echo $e->getMessage();
}