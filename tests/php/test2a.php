<?php
require_once __DIR__ . '/../../src/php/equilibriumIndexes.php';
require_once __DIR__ . '/../../src/php/equilibriumTest.php';

$array = [
    -7, 1, 5, 2, -4, 3, 0
];

echo '<h1>Test Case 2a</h1>';
echo '<p>Existing Equilibriums</p>';
echo '<p>Standard output</p>';
echo '<p>Test array: ' . print_r($array, 1) . '<p><hr>';

var_dump(testEquilibriums($array));