# README #

This repository constitutes the Equilibrium Index code test for Booj. It is written in PHP for version 5.4+, which as of this writing has the highest production saturation of any currently existing PHP distribution. Using the supplied tests in version 5.3 or lower may fail due to the lack of the `__DIR__` magic constant, though this is not used in the actual logic of the functionality. This can be supplanted by replacing instances of `__DIR__` with `realpath(dirname(__FILE__))` as needed. If you are writing your own tests, this should not be a concern.

The actual logic function provided is extensively documented with comments, outlining each step of the operation and the purpose of each variable used within scope. The supplied testing function is not documented, as it has no actual effect on the logic of the operation, and is only provided as a courtesy to view output in a more human-readable way.

This particular problem may be accomplished in a number of different ways. The supplied functionality has been structured as such to take into account performance and readability, and avoids the use of nested loops or functionality such as `array_map` that provide undesirable hits to performance benchmarks.

### How do I get set up? ###

This project may be obtained in the following ways:

Clone directly from git

`git clone git@bitbucket.org:mopsyd/booj-code-test.git`

or alternately [Direct download from the repository page](https://bitbucket.org/mopsyd/booj-code-test)

The project should be copied into a LAMP stack directory running php 5.4+ (most reliable distribution as of this writing).

Include the source file into your testing environment:

`require_once __DIR__ . '/src/php/equilibriumIndexes.php';`

Optionally require the testing function, if you desire to use it:

`require_once __DIR__ . '/src/php/equilibriumTest.php';`

### Dependencies ###

There are no external dependencies for this example. Any server running PHP 5.4+ should suffice for all included materials. The readme file requires a markdown parser to render correctly, which has not been provided as it is out of the scope of the requested functionality, and has no bearing on execution.


### Approach ###

This functionality has been handled with a single function. The author favors an object-oriented approach in general, but due to the simplicity of the required functionality, building a class structure adds unnecessary complexity and ostensibly reduces readability.


### Usage ###

Provide a data source for testing. The supplied data should be a non-associative array with only mathematically complete values (integers, floats, doubles, or booleans). The function performs explicit type checking on the data source, and will throw an exception if the keys are in string format, or if the values include strings, arrays, nulls, objects or resources to prevent bugs arising from malformed data sources being provided.

All examples provided assume your file pointer exists in the root directory of the provided project. Please adjust your paths as necessary if required.

Example usage:

~~~~

<?php

require_once __DIR__ . '/src/php/equilibriumIndexes.php';

$array = array(1, 2, 3, 4, 5, 6, 7, 8, 9);

var_dump(equilibriumIndexes($array));

~~~~

There is also a provided testing function, which can be used to view more verbose output. It does not modify the logic of the original function, but can be used to provide more human-readable results, if desired. This function is entirely optional. If you desire to use it, also include the test function, and pass a second parameter as TRUE.

Example usage with testing function:

~~~~

<?php

require_once __DIR__ . '/src/php/equilibriumIndexes.php';
require_once __DIR__ . '/src/php/equilibriumTest.php';

$array = array(1, 2, 3, 4, 5, 6, 7, 8, 9);

var_dump(equilibriumTest($array, 1));

~~~~

Using the testing function in verbose mode will suppress OutOfBoundsExceptions from terminating execution, and  will render a message stating what the problem with the data source was instead. 

Malformed data sources will throw an out of bounds exception. If you would like to test for this, the approach is the same as above, but should include a try/catch block.

Example of usage with a malformed data source:

~~~~

<?php

require_once __DIR__ . '/src/php/equilibriumIndexes.php';

$array = array(1, 2, 3, new \stdClass(), 5, 6, 7, 8, 9);

try {
    var_dump(equilibriumIndexes($array));
} catch (\Exception $e) {
    echo $e->getMessage();
    exit();
}

~~~~ 


The above example will throw an exception stating that the data source values are incorrectly formatted for the logic of this function. All exception messages are log friendly, providing the specific problem with the data source, as well as the function name, line, and file name where the problem occurred.

### Tests (phpunit) ###

Testing may be done with PHPUnit directly from the command line, as per original assignment instructions.

~~~~

cd path/to/repository

phpunit EquilibriumIndexTest.php

~~~~


### Tests (browser) ###

Test cases are provided with the project. They may be included directly if desired.

* Preexisting tests are available in the tests/php folder
* A testing function has been provided in the src/php folder for common performance and logic testing purposes. It's use is not required to demonstrate usage of the actual function, it is provided as a courtesy to show more robust output in test cases.
* Four tests are included, with two variations each, demonstrating direct output, as well as a more human-readable verbose output. Each of these use the supplied testing function, which may be used by executing the testEquilibriums() function. The first parameter is the array of values to be analyzed, the second parameter is an optional boolean, which if passed as TRUE will return verbose output rather than the default functionality.


### Who do I talk to? ###

Please send any questions or comments to [bdayhoff@gmail.com](mailto:bdayhoff@gmail.com)