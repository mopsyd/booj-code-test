<?php
/**
 * Finds the equilibrium indexes of a provided array.
 * @param array $array an array of numeric values, with only numeric keys.
 * @return array|false Returns false if no equilibrium indexes occur, otherwise returns an array of the indexes.
 * @throws \OutOfBoundsException if associative string keys exist in the provided array, or if a non-mathematically complete value is supplied in the array.
 */
function equilibriumIndexes(array $array) {
    
    /*
     * Since PHP treats all arrays as associative, possible bugs may arise if 
     * the function assumes that all provided arguments have numeric keys.
     * Explicit checking prevents this error from occurring.
     * @see http://php.net/manual/en/language.types.string.php#language.types.string.conversion
     * 
     * Out of bounds exceptions indicate an error with a particular key, 
     * which cannot neccessarily be determined at compile time 
     * 
     * (although PHP is an interpreted language, 
     * the parser does actually compile into 
     * optimized C code in cached memory, 
     * though this typically is opaque to the 
     * development process). 
     * 
     * Since array keys may only be strings or integers, 
     * the type checking here does not need to be as robust as for values.
     */
    if (count(array_filter(array_keys($array), 'is_string')) != 0) {
        throw new \OutOfBoundsException('Provided array parameter may only have numeric keys!' 
                . PHP_EOL . '>At ' . __FUNCTION__ . ', in line: ' . __LINE__ . ' of ' . __FILE__, 1 );
    } 
    
    /*
     * Likewise, the function cannot proceed if non-numeric values are provided. 
     * Explicit type checking prevents this sort of error from occurring. 
     * This filters only types that cannot be mathmatically computed, 
     * allowing integers, floats, and doubles to remain.
     * 
     * PHP language constructs operate significantly faster than 
     * custom functions or closures, so they have been used in place of 
     * a custom checking function for performance optimization.
     * 
     * Checking has been filtered in order of likelihood of occurrance to avoid 
     * unneccessary additional checks in the event that a faulty value is detected.
     */
    elseif (count(array_filter($array, 'is_string')) != 0
              || count(array_filter($array, 'is_null')) != 0
              || count(array_filter($array, 'is_array')) != 0
              || count(array_filter($array, 'is_object')) != 0
              || count(array_filter($array, 'is_resource')) != 0
            ) {
        throw new \OutOfBoundsException('Provided array parameter may only have numeric values!' 
                . PHP_EOL . '>At ' . __FUNCTION__ . ', in line: ' . __LINE__ . ' of ' . __FILE__, 2 );
    }
    
    
    /*
     * This variable contains the sum of 
     * all values in the array that exist 
     * after the current test case. 
     * 
     * Before any operations have occurred, 
     * this equates to all values in the array.
     */
    $right = array_sum($array);
    
    /*
     * This variable contains the sum of 
     * all values in the array that exist 
     * prior to the current test case. 
     * 
     * Before any operations have occurred, 
     * this has a zero value.
     */
    $left = 0;
    
    /*
     * This variable is a container for 
     * any matched equilibriums found 
     * during iteration.
     */
    $equilibriums = [];
    
    /**
     * There are many ways to approach the iteration 
     * through the provided dataset, which may include 
     * array_map(), for, while(), etc.
     * 
     * If one were to benchmark mircotimes, it is possible to 
     * gain a negligible performance gain using do/while if it is 
     * correctly structured, however microtime benchmarks add 
     * significant development time for negligible gains, and 
     * are generally bad practice. It would take several million iterations 
     * for any measurable gain to occur, which would in the 
     * majority of cases trip the PHP memory limit or max time execution 
     * long before occurring, and should be generally handled by another 
     * layer of logic in the stack that is not subject to such restrictions 
     * (ie: python, SQL, java, etc.). while() and array_map() both 
     * perform slower than foreach, and add a layer of 
     * unneccessary readability overhead. I opted to go with 
     * foreach for this reason, due to good performance 
     * and ease of readability.
     * @see http://stackoverflow.com/questions/8081253/do-while-is-the-fastest-loop-in-php/8081402#8081402
     */
    foreach ($array as $key => $value) {
        
        /**
         * In an effort to avoid nested loops, 
         * which add significant overhead to execution, 
         * some basic math is applied 
         * to avoid unneccessary operations that come 
         * from looping through the remaining irrelevant 
         * results once the desired check has occurred.
         */
        
        /*
         * First, the current iteration value is subtracted 
         * from the sum of values that occur after the 
         * current test case iteration.
         */
        $right -= $value;
        
        /*
         * The sum of the previous iterations is 
         * then checked against the remaining sum.
         */
        if ($left == $right) {
            /*
             * If a match occurs, the current key is added to
             * the equilibrium index subset.
             */
            $equilibriums[] = $key;
        }
        
        /*
         * The current iteration value is then 
         * added to the prior sum to prepare for 
         * the next iteration.
         */
        $left += $value;
    }
    
    /**
     * In the event that no equilibrium indexes occur whatsoever, 
     * explicitly returns FALSE. 
     * 
     * Otherwise an array of the equilibrium keys is returned.
     */
    return ( (!empty($equilibriums) ) ? $equilibriums : FALSE);
}