<?php
/**
 * Test Function
 * @param array $array The array to test
 * @param boolean $verbose (optional) If omitted or FALSE, returns direct output. If TRUE, returns a verbose statement of the results.
 * @return void
 */
function testEquilibriums($array, $verbose = FALSE) {
    try {
        $result = equilibriumIndexes($array);
        if (!$result) {
            $message = (($verbose) 
                    ? "There are no equilibrium indexes in the supplied array." 
                    : $result);
        } else {
            $message = (($verbose) 
                    ? "Equilibrium indexes exist at the following keys for the supplied array: " . implode(', ', $result) 
                    : $result);
        }
    } catch (\OutOfBoundsException $e) {
        if (!$verbose) {
            throw $e;
        }
        $message = "The operation cannot proceed because the supplied array contains "
        . (($e->getCode() == 1) 
                ? "invalid keys (keys must be numeric)." 
                : "non-mathematically complete values (acceptable values are int, float, boolean, or double).");
    }
    return $message;
}